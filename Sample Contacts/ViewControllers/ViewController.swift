//
//  ViewController.swift
//  Sample Contacts
//
//  Created by coeus on 16/01/2021.
//

import UIKit
import CoreData

class DataModel
{
    var title : String
    var discipline : String
    var url: String
    var isFav : Bool
    
    init(title: String, discipline: String, url: String, fav: Bool) {
        self.title = title
        self.discipline = discipline
        self.url = url
        self.isFav = fav
    }
}

class ViewController: UIViewController
{
    //Connections
    @IBOutlet weak private var contactsTableView: UITableView!
    
    
    //Variables
    private let searchController = UISearchController(searchResultsController: nil)
    private var navBarTitle: String = "All Data"
    
    private var filteredArray: [DataModel] = [] // Array for search filter
    private var favArray: [DataModel] = [] //Array for favorite Elements
    var namesDictionary = [String: [DataModel]]() //creating Dictionary like ["A" : ["Allen"]]
    var hiddenSections : [Int] = []
    
    //Array for DataModel
    var data: [DataModel] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        data.append(DataModel(title: "Test", discipline: "Tattoo Artistry", url: "https://www.google.com", fav: false))
        data.append(DataModel(title: "Test1", discipline: "Candle Shop", url: "https://www.google.com", fav: true))
        data.append(DataModel(title: "Test2", discipline: "Candle Shop", url: "https://www.google.com", fav: false))
        data.append(DataModel(title: "Check", discipline: "Pumpkin Patch Farm", url: "https://www.google.com", fav: false))
        data.append(DataModel(title: "John", discipline: "Wine & Spirits", url: "https://www.google.com", fav: false))
        data.append(DataModel(title: "Allen", discipline: "Tattoo Artistry", url: "https://www.google.com", fav: false))
        
        setUpNavBar()
        createNameDictionary() // Creating Dictionary for Alphabets which create shows in table view header like
                                // A : "Allen" "Andrew"
    }
    
    private func setUpNavBar() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = navBarTitle
        
        self.navigationItem.searchController = searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
    
        searchController.searchResultsUpdater = self as UISearchResultsUpdating
        definesPresentationContext = true
    }
    
    func createNameDictionary()
    {
        for x in data {
            let name = x.discipline
            
            if (namesDictionary.keys.contains { (keyName) -> Bool in
                if (keyName == name) { return true }
                return false })
            {
                namesDictionary[name]?.append(DataModel.init(title: x.title, discipline: x.discipline, url: x.url, fav: x.isFav))
            }
            else
            {
                namesDictionary[name] = [x]
            }
        }
        checkFavoriteCount()
    }
    
    func checkFavoriteCount()
    {
        favArray.removeAll()
        for x in (0...namesDictionary.keys.count - 1)
        {
            let k = namesDictionary[x].key
            for y in (0...namesDictionary[x].value.count - 1)
            {
                if (namesDictionary[k]?[y].isFav ?? false)
                {
                    favArray.append((namesDictionary[k]?[y])!)
                }
            }
        }
        contactsTableView.reloadData()
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource
{
    public func numberOfSections(in tableView: UITableView) -> Int {
        var count = Int()
        
        if searchController.isActive && searchController.searchBar.text != "" {
            count = 1
        }
        
        else if (favArray.count != 0 )
        {
            count = namesDictionary.keys.count + 1
        }
        
        else {
            count = namesDictionary.keys.count
        }
        
        return count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = Int()
        
        if self.hiddenSections.contains(section) {
            return 0
        }
        else
        {
            if searchController.isActive && searchController.searchBar.text != "" {
                count = filteredArray.count
            }
            
            else if (favArray.count != 0){
                
                if (section == 0)
                {
                    count = favArray.count
                }
                else
                {
                    count = Array(namesDictionary)[section - 1].value.count
                }
            }
            
            else {
                count = Array(namesDictionary)[section].value.count
            }
            
            return count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell", for: indexPath) as? ContactsCell else { return UITableViewCell() }
        
        var names : String
        var dicData : DataModel?
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.contactName.text = filteredArray[indexPath.row].title
        }
        
        
        else if (favArray.count != 0)
        {
            if (indexPath.section == 0)
            {
                cell.contactName.text = favArray[indexPath.row].title
                cell.favBtn.setImage(UIImage(named: "starIcon"), for: .normal)
                cell.favBtn.isEnabled = false
            }
            else
            {
                cell.favBtn.isEnabled = true
                names = namesDictionary[indexPath.section - 1].key
                dicData = namesDictionary[names]?[(indexPath.row)]
                cell.contactName.text = dicData?.title
                
                if (dicData?.isFav ?? false)
                {
                    cell.favBtn.setImage(UIImage(named: "starIcon"), for: .normal)
                }
                else
                {
                    cell.favBtn.setImage(UIImage(named: "starUnfilled"), for: .normal)
                }
                
                cell.favBtn.value = dicData
                cell.favBtn.index = indexPath
                cell.favBtn.addTarget(self, action: #selector(favBtn(_:)), for: .touchUpInside)
            }
        }
        
        else
        {
            cell.favBtn.isEnabled = true
            names = namesDictionary[indexPath.section].key
            dicData = namesDictionary[names]?[indexPath.row]
            cell.contactName.text = dicData?.title
            
            if (dicData?.isFav ?? false)
            {
                cell.favBtn.setImage(UIImage(named: "starIcon"), for: .normal)
            }
            else
            {
                cell.favBtn.setImage(UIImage(named: "starUnfilled"), for: .normal)
            }
            
            cell.favBtn.value = dicData
            cell.favBtn.index = indexPath
            cell.favBtn.addTarget(self, action: #selector(favBtn(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    @objc func favBtn(_ sender: CustomButton)
    {
        let index = sender.index
        let cell = contactsTableView.cellForRow(at: index!) as! ContactsCell
        let dicData = sender.value
        
        if (dicData?.isFav ?? false)
        {
            cell.favBtn.setImage(UIImage(named: "starUnfilled"), for: .normal)
            dicData?.isFav = false
            self.checkFavoriteCount()
        }
        else
        {
            cell.favBtn.setImage(UIImage(named: "starIcon"), for: .normal)
            dicData?.isFav = true
            self.checkFavoriteCount()
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let url = URL(string: data[indexPath.row].url){
            UIApplication.shared.open(url)
        }
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            return false
        }
        
        return true
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionButton = UIButton()
            
        if searchController.isActive && searchController.searchBar.text != "" {
            sectionButton.setTitle(String("Top Name Matches"),for: .normal)
        }
        else if (favArray.count != 0){
            if (section == 0)
            {
                sectionButton.setTitle(String("Favorites"),for: .normal)
            }
            else {
                sectionButton.setTitle(String(Array(namesDictionary)[section - 1].key),for: .normal)
            }
        }
        else {
            sectionButton.setTitle(String(Array(namesDictionary)[section].key),for: .normal)
        }
            
        sectionButton.backgroundColor = .systemGray4
        sectionButton.tag = section
        sectionButton.addTarget(self,action: #selector(self.hideSection(sender:)),for: .touchUpInside)
        return sectionButton
    }
    
    @objc private func hideSection(sender: UIButton) {
        let section = sender.tag
        if hiddenSections.contains(section)
        {
            hiddenSections.removeAll { (i) -> Bool in
                if (i == section) {return true}
                else {return false}
            }
        }
        else
        {
            hiddenSections.append(section)
        }
        contactsTableView.reloadData()
    }
}
extension ViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContacts(text: searchController.searchBar.text!)
    }
    
    private func filterContacts(text: String, scope: String = "All") {
        
        filteredArray = data.filter({ (d) -> Bool in
            return d.title.lowercased().contains(text.lowercased())
        })
        
        contactsTableView.reloadData()
    }
}

extension Dictionary
{
    subscript (i: Int) -> (key: Key, value: Value)
    {
        get{
            return self[index(startIndex, offsetBy: i)]
        }
    }
    
}
